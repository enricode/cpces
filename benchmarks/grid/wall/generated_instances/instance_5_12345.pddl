;; Enrico Scala <enricos83@gmail.com> and Alban Grastien <Alban.Grastien@data61.csiro.au>
(define (problem instance_5__12345)
(:domain grid-wall)

(:init
        (and
                (oneof (x zero)(x one)(x two)(x three)(x four))
				(oneof (y zero)(y one)(y two)(y three)(y four))
				(not (and (x four) (y two)))
                (not (canmove))
        )
)

(:goal (and (and (x two) (y zero)) )))

