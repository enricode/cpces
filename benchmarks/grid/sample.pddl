(define (problem grid_4_4)
(:domain dispose)
   (:init
    (and
        (oneof
            (x one)
            (x two)
            (x three) 
            (x four)             
        ) 
        (oneof
            (y one)
            (y two)
            (y three) 
            (y four)
        )          

    )
    )
    (:goal (and (x two) (y two))))
