;; Enrico Scala <enricos83@gmail.com> and Alban Grastien <Alban.Grastien@data61.csiro.au>
(define (problem instance_29__12345)
(:domain grid-wall)

(:init
        (and
                (oneof (x zero)(x one)(x two)(x three)(x four)(x five)(x six)(x seven)(x eight)(x nine)(x ten)(x eleven)(x twelve)(x thirteen)(x fourteen)(x fifteen)(x sixteen)(x seventeen)(x eighteen)(x nineteen)(x twenty)(x twenty-one)(x twenty-two)(x twenty-three)(x twenty-four)(x twenty-five)(x twenty-six)(x twenty-seven)(x twenty-eight))
				(oneof (y zero)(y one)(y two)(y three)(y four)(y five)(y six)(y seven)(y eight)(y nine)(y ten)(y eleven)(y twelve)(y thirteen)(y fourteen)(y fifteen)(y sixteen)(y seventeen)(y eighteen)(y nineteen)(y twenty)(y twenty-one)(y twenty-two)(y twenty-three)(y twenty-four)(y twenty-five)(y twenty-six)(y twenty-seven)(y twenty-eight))
				(not (and (x twenty-four) (y nine)))
                (not (canmove))
        )
)

(:goal (and (and (x twelve) (y zero)) )))

