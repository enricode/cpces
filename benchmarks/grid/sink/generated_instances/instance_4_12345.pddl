;; Enrico Scala <enricos83@gmail.com> and Alban Grastien <Alban.Grastien@data61.csiro.au>
(define (problem instance_4__12345)
(:domain grid-wall)

(:init
        (and
                (oneof (x zero)(x one)(x two)(x three))
				(oneof (y zero)(y one)(y two)(y three))
				(not (and (x three) (y one)))
                (not (canmove))
        )
)

(:goal (and (and (x two) (y zero)) )))

