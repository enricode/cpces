;; Enrico Scala <enricos83@gmail.com> and Alban Grastien <Alban.Grastien@data61.csiro.au>
(define (problem instance_3__12345)
(:domain grid-wall)

(:init
        (and
                (oneof (x zero)(x one)(x two))
				(oneof (y zero)(y one)(y two))
				(not (and (x two) (y one)))
                (not (canmove))
        )
)

(:goal (and (and (x one) (y zero)) )))

