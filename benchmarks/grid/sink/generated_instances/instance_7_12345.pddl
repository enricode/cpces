;; Enrico Scala <enricos83@gmail.com> and Alban Grastien <Alban.Grastien@data61.csiro.au>
(define (problem instance_7__12345)
(:domain grid-wall)

(:init
        (and
                (oneof (x zero)(x one)(x two)(x three)(x four)(x five)(x six))
				(oneof (y zero)(y one)(y two)(y three)(y four)(y five)(y six))
				(not (and (x five) (y two)))
                (not (canmove))
        )
)

(:goal (and (and (x three) (y zero)) )))

