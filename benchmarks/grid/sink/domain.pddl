
(define (domain grid)

   (:requirements :strips :typing)
   (:types num )
   (:constants one two three four -num)
   (:predicates (x ?i - num) (y ?i - num) (hell ?i ?j - num) (canmove) )

   (:action can_move
      :parameters()
      :precondition()
      :effect (and
                  (when (and (x two) (y two)) (not (canmove)))
                  (when (not (and (x two) (y two))) (canmove))))
   
   (:action north
      :parameters ()
      :precondition ()
      :effect (and
                  ;;next is 
                  (when (and (y two) (canmove)) (and (y one) (not (y two))))
                  (when (and (y three) (canmove)) (and (y two) (not (y three))))
                  (when (and (y four) (canmove)) (and (y three) (not (y four))))
                  (not (canmove))))                 
   (:action south
       :parameters ()
      :precondition ()
      :effect (and
                  ;;next is 
                  (when (and (y one)(canmove)) (and (y two) (not (y one))))
                  (when (and (y two)(canmove)) (and (y three) (not (y two))))
                  (when (and (y three)(canmove)) (and (y four) (not (y three))))                  
                  (not (canmove))))                 
   (:action east
      :parameters ()
      :precondition ()
      :effect (and
                  ;;next is 
                  (when (and (x one)(canmove)) (and (x two) (not (x one))))
                  (when (and (x two)(canmove)) (and (x three) (not (x two))))
                  (when (and (x three)(canmove)) (and (x four) (not (x three))))
                  (not (canmove))))                 
   (:action west
      :parameters ()
      :precondition ()
      :effect (and
                  ;;next is 
                  (when (and (x two)(canmove)) (and (x one) (not (x two))))
                  (when (and (x three)(canmove)) (and (x two) (not (x three))))
                  (when (and (x four)(canmove)) (and (x three) (not (x four))))
                  (not (canmove)))))                 
