
(define (domain grid)

   (:requirements :strips :typing)
   (:types num )
   (:constants one two three four -num)
   (:predicates (x ?i - num) (y ?i - num) (hell ?i ?j - num) )

   
   (:action north
      :parameters ()
      :precondition ()
      :effect (and
                  ;;next is 
                  (when (y two) (and (y one) (not (y two))))
                  (when (y three) (and (y two) (not (y three))))
                  (when (y four) (and (y three) (not (y four))))))                  
   (:action south
       :parameters ()
      :precondition ()
      :effect (and
                  ;;next is 
                  (when (y one) (and (y two) (not (y one))))
                  (when (y two) (and (y three) (not (y two))))
                  (when (y three) (and (y four) (not (y three))))))                  
                  
   (:action east
      :parameters ()
      :precondition ()
      :effect (and
                  ;;next is 
                  (when (x one) (and (x two) (not (x one))))
                  (when (x two) (and (x three) (not (x two))))
                  (when (x three) (and (x four) (not (x three))))))                  
   (:action west
      :parameters ()
      :precondition ()
      :effect (and
                  ;;next is 
                  (when (x two) (and (x one) (not (x two))))
                  (when (x three) (and (x two) (not (x three))))
                  (when (x four) (and (x three) (not (x four)))))))                 
